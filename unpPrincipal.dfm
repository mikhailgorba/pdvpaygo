object frmPrincipal: TfrmPrincipal
  Left = 0
  Top = 0
  Caption = 'Caixa Pay&GO'
  ClientHeight = 728
  ClientWidth = 1157
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 241
    Top = 0
    Width = 447
    Height = 728
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 23
      Top = 39
      Width = 103
      Height = 13
      Caption = '003-000 Valor total M'
    end
    object Label10: TLabel
      Left = 25
      Top = 306
      Width = 176
      Height = 13
      Caption = '706-000 Capacidades da Automa'#231#227'o'
    end
    object Label11: TLabel
      Left = 25
      Top = 414
      Width = 167
      Height = 13
      Caption = '716-000 Empresa da Automa'#231#227'o M'
    end
    object Label12: TLabel
      Left = 25
      Top = 501
      Width = 148
      Height = 13
      Caption = '733-000 Vers'#227'o da interface M'
    end
    object Label13: TLabel
      Left = 23
      Top = 528
      Width = 156
      Height = 13
      Caption = '735-000 Nome da Automa'#231#227'o M '
    end
    object Label14: TLabel
      Left = 25
      Top = 554
      Width = 159
      Height = 13
      Caption = '736-000 Vers'#227'o da Automa'#231#227'o M'
    end
    object Label15: TLabel
      Left = 25
      Top = 582
      Width = 172
      Height = 13
      Caption = '738-000 Registro de Certifica'#231#227'o M '
    end
    object Label16: TLabel
      Left = 25
      Top = 608
      Width = 149
      Height = 13
      Caption = '999-999 Registro finalizador M '
    end
    object Label17: TLabel
      Left = 213
      Top = 120
      Width = 25
      Height = 13
      Caption = 'CNPJ'
    end
    object Label18: TLabel
      Left = 213
      Top = 144
      Width = 19
      Height = 13
      Caption = 'CPF'
    end
    object Label19: TLabel
      Left = 23
      Top = 635
      Width = 156
      Height = 13
      Caption = '749-000 Forma de pagamento O'
    end
    object Label2: TLabel
      Left = 23
      Top = 12
      Width = 112
      Height = 13
      Caption = '001-000 Id da venda M'
    end
    object Label20: TLabel
      Left = 23
      Top = 662
      Width = 172
      Height = 13
      Caption = '738-000 Registro de Certifica'#231#227'o M '
    end
    object Label3: TLabel
      Left = 25
      Top = 225
      Width = 133
      Height = 13
      Caption = '018-000 Qtde. parcelas C2 '
    end
    object Label4: TLabel
      Left = 23
      Top = 93
      Width = 183
      Height = 13
      Caption = '006-000 Ent Cliente (Fisica/Juridica) O'
    end
    object Label5: TLabel
      Left = 23
      Top = 171
      Width = 135
      Height = 13
      Caption = '010-000 Rede Adquirente O'
    end
    object Label6: TLabel
      Left = 23
      Top = 66
      Width = 89
      Height = 13
      Caption = '004-000 Moeda M '
    end
    object Label9: TLabel
      Left = 23
      Top = 120
      Width = 154
      Height = 13
      Caption = '007-000 Identificador Cliente O '
    end
    object Label8: TLabel
      Left = 23
      Top = 198
      Width = 161
      Height = 13
      Caption = '732-000 Tipo de Financiamento O'
    end
    object Label21: TLabel
      Left = 25
      Top = 249
      Width = 134
      Height = 13
      Caption = '024-000 Data pr'#233'-datado O'
    end
    object Label22: TLabel
      Left = 25
      Top = 279
      Width = 149
      Height = 13
      Caption = '702-000 Id do Estabelecimento'
    end
    object Label23: TLabel
      Left = 25
      Top = 473
      Width = 123
      Height = 13
      Caption = '731-000 Tipo de cart'#227'o O'
    end
    object Label24: TLabel
      Left = 25
      Top = 333
      Width = 123
      Height = 13
      Caption = '708-000 Valor do Troco O'
    end
    object Label25: TLabel
      Left = 25
      Top = 357
      Width = 141
      Height = 13
      Caption = '709-000 Valor do Desconto O'
    end
    object Label26: TLabel
      Left = 25
      Top = 387
      Width = 103
      Height = 13
      Caption = '743-000 Valor Devido'
    end
    object Label27: TLabel
      Left = 25
      Top = 441
      Width = 90
      Height = 13
      Caption = '730-000 Opera'#231#227'o'
    end
    object Label28: TLabel
      Left = 23
      Top = 692
      Width = 185
      Height = 13
      Caption = '750-000 identifica'#231#227'o carteira digital O'
    end
    object cb004000: TComboBox
      Left = 212
      Top = 63
      Width = 215
      Height = 21
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 0
      Text = '0 - Real'
      Items.Strings = (
        '0 - Real'
        '1 - D'#243'lar americano'
        '2 - Euro')
    end
    object cb006000: TComboBox
      Left = 212
      Top = 90
      Width = 215
      Height = 21
      Style = csDropDownList
      Enabled = False
      ItemIndex = 0
      TabOrder = 1
      Text = 'F'#237'sica      (CPF)'
      OnChange = cb006000Change
      Items.Strings = (
        'F'#237'sica      (CPF)'
        'Jur'#237'dica (CNPJ)')
    end
    object cb749000: TComboBox
      Left = 212
      Top = 632
      Width = 214
      Height = 21
      ItemIndex = 0
      TabOrder = 2
      Text = '1: cart'#227'o'
      Items.Strings = (
        '1: cart'#227'o'
        '2: dinheiro'
        '4: cheque'
        '8: carteira digital')
    end
    object edt706000: TEdit
      Left = 212
      Top = 303
      Width = 215
      Height = 21
      TabOrder = 3
      Text = '510'
    end
    object edt001000: TEdit
      Left = 212
      Top = 9
      Width = 215
      Height = 21
      MaxLength = 9
      NumbersOnly = True
      ParentShowHint = False
      ShowHint = False
      TabOrder = 4
      Text = '179'
    end
    object edt003000: TEdit
      Left = 212
      Top = 36
      Width = 215
      Height = 21
      MaxLength = 12
      NumbersOnly = True
      TabOrder = 5
      Text = '99999999'
    end
    object edt007000Cnpj: TEdit
      Left = 256
      Top = 117
      Width = 171
      Height = 21
      Enabled = False
      MaxLength = 16
      NumbersOnly = True
      TabOrder = 6
      Text = '00898594000108'
    end
    object edt007000Cpf: TEdit
      Left = 256
      Top = 141
      Width = 171
      Height = 21
      Enabled = False
      MaxLength = 11
      TabOrder = 7
      Text = '04198898170'
    end
    object edt010000: TEdit
      Left = 212
      Top = 168
      Width = 215
      Height = 21
      Enabled = False
      MaxLength = 8
      TabOrder = 8
      Text = 'REDECARD'
    end
    object edt018000: TEdit
      Left = 213
      Top = 222
      Width = 215
      Height = 21
      Enabled = False
      MaxLength = 2
      NumbersOnly = True
      TabOrder = 9
      Text = '1'
    end
    object edt716000: TEdit
      Left = 212
      Top = 411
      Width = 215
      Height = 21
      TabOrder = 10
      Text = 'WP AUTOMA'#199#195'O DE ACESSOS'
    end
    object edt733000: TEdit
      Left = 212
      Top = 497
      Width = 215
      Height = 21
      TabOrder = 11
      Text = '224'
    end
    object edt735000: TEdit
      Left = 212
      Top = 524
      Width = 215
      Height = 21
      TabOrder = 12
      Text = 'CaixaPay&GO'
    end
    object edt736000: TEdit
      Left = 212
      Top = 551
      Width = 215
      Height = 21
      TabOrder = 13
      Text = 'V1'
    end
    object edt738000: TEdit
      Left = 212
      Top = 578
      Width = 215
      Height = 21
      TabOrder = 14
      Text = 'G45J35G3JH45B435'
    end
    object edt999999: TEdit
      Left = 212
      Top = 605
      Width = 215
      Height = 21
      TabOrder = 15
      Text = '0'
    end
    object Edit1: TEdit
      Left = 212
      Top = 659
      Width = 215
      Height = 21
      TabOrder = 16
      Text = '0'
    end
    object cb732000: TComboBox
      Left = 213
      Top = 195
      Width = 214
      Height = 21
      Enabled = False
      ItemIndex = 0
      TabOrder = 17
      Text = '0: qualquer / n'#227'o definido (padr'#227'o)'
      OnChange = cb732000Change
      Items.Strings = (
        '0: qualquer / n'#227'o definido (padr'#227'o)'
        '1: '#224' vista'
        '2: parcelado pelo Emissor'
        '3: parcelado pelo Estabelecimento'
        '4: pr'#233'-datado'
        '5: pr'#233'-datado for'#231'ado')
    end
    object edt024000: TDateTimePicker
      Left = 213
      Top = 249
      Width = 213
      Height = 21
      Date = 44123.434875057870000000
      Time = 44123.434875057870000000
      Enabled = False
      TabOrder = 18
    end
    object edt702000: TEdit
      Left = 213
      Top = 276
      Width = 215
      Height = 21
      Enabled = False
      MaxLength = 2
      TabOrder = 19
      Text = '1'
    end
    object edt708000: TEdit
      Left = 212
      Top = 330
      Width = 215
      Height = 21
      Enabled = False
      TabOrder = 20
      Text = '0'
    end
    object edt743000: TEdit
      Left = 212
      Top = 384
      Width = 215
      Height = 21
      Enabled = False
      TabOrder = 21
      Text = '0'
    end
    object edt709000: TEdit
      Left = 212
      Top = 357
      Width = 215
      Height = 21
      Enabled = False
      TabOrder = 22
      Text = '0'
    end
    object cb750000: TComboBox
      Left = 214
      Top = 686
      Width = 214
      Height = 21
      Enabled = False
      ItemIndex = 0
      TabOrder = 23
      Text = '001: QRCode do checkout (lido pelo celular do portador)'
      Items.Strings = (
        '001: QRCode do checkout (lido pelo celular do portador)'
        '002: CPF'
        '128: outra')
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 241
    Height = 728
    Align = alLeft
    TabOrder = 1
    object ch006000: TCheckBox
      Left = 10
      Top = 27
      Width = 225
      Height = 17
      Caption = 'Entidade Cliente? CPF/CNPJ'
      TabOrder = 0
    end
    object ch702000: TCheckBox
      Left = 10
      Top = 248
      Width = 225
      Height = 17
      Caption = 'Transa'#231#245'es para v'#225'rios estabelecimentos?'
      TabOrder = 1
    end
    object ch010000: TCheckBox
      Left = 10
      Top = 92
      Width = 225
      Height = 17
      Caption = 'Informa Rede Adquirente?'
      TabOrder = 2
    end
    object ch732000: TCheckBox
      Left = 10
      Top = 170
      Width = 225
      Height = 17
      Caption = 'Parcelado?'
      TabOrder = 3
    end
    object Button9: TButton
      Left = 10
      Top = 674
      Width = 225
      Height = 45
      Caption = 'Salvar configura'#231#227'o'
      TabOrder = 4
      OnClick = Button9Click
    end
    object ch731000: TCheckBox
      Left = 10
      Top = 332
      Width = 225
      Height = 17
      Caption = 'Tipo de cartao'
      TabOrder = 5
    end
    object ch708000: TCheckBox
      Left = 10
      Top = 413
      Width = 225
      Height = 17
      Caption = 'Transa'#231#227'o com Troco?'
      TabOrder = 6
    end
    object ch730000: TCheckBox
      Left = 10
      Top = 490
      Width = 225
      Height = 17
      Caption = 'Pergunta qual Opera'#231#227'o?'
      TabOrder = 7
    end
    object ch709000: TCheckBox
      Left = 10
      Top = 569
      Width = 225
      Height = 17
      Caption = 'Pergunta qual o Deconto?'
      TabOrder = 8
    end
    object ch750000: TCheckBox
      Left = 10
      Top = 651
      Width = 225
      Height = 17
      Caption = 'Carteira digital?'
      TabOrder = 9
    end
  end
  object Panel4: TPanel
    Left = 688
    Top = 0
    Width = 469
    Height = 728
    Align = alRight
    TabOrder = 2
    object Button1: TButton
      Left = 278
      Top = 182
      Width = 179
      Height = 25
      Caption = 'Gerar Req\intpos.001 ADM/CRT'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button3: TButton
      Left = 277
      Top = 213
      Width = 180
      Height = 25
      Caption = 'Ler Resp\intpos.sts'
      TabOrder = 1
      OnClick = Button3Click
    end
    object Button4: TButton
      Left = 277
      Top = 244
      Width = 180
      Height = 25
      Caption = 'Ler Status'
      TabOrder = 2
      OnClick = Button4Click
    end
    object mmReqIntPos001: TMemo
      Left = 6
      Top = 150
      Width = 265
      Height = 276
      TabOrder = 3
    end
    object Panel1: TPanel
      Left = 280
      Top = 9
      Width = 185
      Height = 135
      TabOrder = 4
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 98
        Height = 13
        Caption = 'Status da opera'#231#227'o:'
      end
      object lbStatus: TLabel
        Left = 16
        Top = 35
        Width = 119
        Height = 13
        Caption = 'Aguardando opera'#231#227'o...'
      end
    end
    object Button5: TButton
      Left = 276
      Top = 275
      Width = 181
      Height = 25
      Caption = 'ATV'
      TabOrder = 5
      OnClick = Button5Click
    end
    object btnCnc: TButton
      Left = 278
      Top = 370
      Width = 179
      Height = 25
      Caption = 'CNC'
      TabOrder = 6
      OnClick = btnCncClick
    end
    object Button7: TButton
      Left = 279
      Top = 339
      Width = 178
      Height = 25
      Caption = 'NCN'
      Enabled = False
      TabOrder = 7
    end
    object Button8: TButton
      Left = 278
      Top = 308
      Width = 179
      Height = 25
      Caption = 'CDP'
      TabOrder = 8
      OnClick = Button8Click
    end
    object mmResp: TMemo
      Left = 6
      Top = 432
      Width = 451
      Height = 273
      TabOrder = 9
      OnChange = mmRespChange
    end
    object mmStatus: TMemo
      Left = 6
      Top = 9
      Width = 265
      Height = 135
      TabOrder = 10
      OnChange = mmStatusChange
    end
    object btCnf: TButton
      Left = 279
      Top = 401
      Width = 178
      Height = 25
      Caption = 'CNF'
      TabOrder = 11
      OnClick = btCnfClick
    end
    object cbOperacao: TComboBox
      Left = 277
      Top = 155
      Width = 180
      Height = 21
      ItemIndex = 0
      TabOrder = 12
      Text = 'ADM'
      Items.Strings = (
        'ADM'
        'CRT')
    end
  end
  object cb731000: TComboBox
    Left = 453
    Top = 470
    Width = 214
    Height = 21
    Enabled = False
    TabOrder = 3
    Text = '0: qualquer / n'#227'o definido (padr'#227'o)'
    OnChange = cb732000Change
    Items.Strings = (
      '0: qualquer / n'#227'o definido (padr'#227'o)'
      '1: cr'#233'dito'
      '2: d'#233'bito'
      '3: voucher')
  end
  object cb730000: TComboBox
    Left = 454
    Top = 438
    Width = 214
    Height = 21
    Enabled = False
    TabOrder = 4
    Text = '01: venda (pagamento de mercadoria/servi'#231'o)'
    OnChange = cb732000Change
    Items.Strings = (
      '01: venda (pagamento de mercadoria/servi'#231'o)'
      '51: cancelamento de venda / reembolso'
      '02: pr'#233'-autoriza'#231#227'o'
      '52: cancelamento de pr'#233'-autoriza'#231#227'o'
      '03: consulta do cart'#227'o (de saldo, financiamento, etc.)'
      '04: consulta de cheque'
      '05: garantia de cheque'
      '06: saque'
      '08: doa'#231#227'o (em dinheiro)'
      '09: doa'#231#227'o (com cart'#227'o)'
      '10: pagamento de conta/boleto/fatura (em dinheiro)'
      '60: cancelamento de pagamento de conta (dinheiro)'
      '11: pagamento de conta/boleto/fatura (com cart'#227'o)'
      '61: cancelamento de pagamento de conta (cart'#227'o)'
      '12: compra de cr'#233'ditos / recarga de celular pr'#233'-pago (em'
      'dinheiro)'
      '13: compra de cr'#233'ditos / recarga de celular pr'#233'-pago (com'
      'cart'#227'o)'
      '48: fechamento/finaliza'#231#227'o'
      '49: outra opera'#231#227'o administrativa')
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 7000
    OnTimer = Timer1Timer
    Left = 193
    Top = 600
  end
  object TimerGerarADM: TTimer
    Enabled = False
    OnTimer = TimerGerarADMTimer
    Left = 193
    Top = 640
  end
  object TimerLerResp: TTimer
    Enabled = False
    OnTimer = TimerLerRespTimer
    Left = 193
    Top = 680
  end
end
