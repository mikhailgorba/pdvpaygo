﻿unit unpPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Mask,
  Vcl.ComCtrls;

type
  TfrmPrincipal = class(TForm)
    edt001000: TEdit;
    edt003000: TEdit;
    edt007000Cnpj: TEdit;
    edt010000: TEdit;
    Button1: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Panel1: TPanel;
    Label7: TLabel;
    lbStatus: TLabel;
    Button3: TButton;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    edt018000: TEdit;
    edt706000: TEdit;
    edt716000: TEdit;
    edt733000: TEdit;
    edt735000: TEdit;
    edt736000: TEdit;
    edt738000: TEdit;
    edt999999: TEdit;
    mmReqIntPos001: TMemo;
    cb006000: TComboBox;
    cb004000: TComboBox;
    Button4: TButton;
    edt007000Cpf: TEdit;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    cb749000: TComboBox;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Button5: TButton;
    btnCnc: TButton;
    Button7: TButton;
    Button8: TButton;
    ch006000: TCheckBox;
    ch702000: TCheckBox;
    ch010000: TCheckBox;
    ch732000: TCheckBox;
    Edit1: TEdit;
    Label8: TLabel;
    cb732000: TComboBox;
    Button9: TButton;
    Label21: TLabel;
    edt024000: TDateTimePicker;
    Label22: TLabel;
    edt702000: TEdit;
    Timer1: TTimer;
    TimerGerarADM: TTimer;
    TimerLerResp: TTimer;
    mmResp: TMemo;
    mmStatus: TMemo;
    btCnf: TButton;
    cbOperacao: TComboBox;
    Label23: TLabel;
    ch731000: TCheckBox;
    Label24: TLabel;
    edt708000: TEdit;
    ch708000: TCheckBox;
    Label25: TLabel;
    Label26: TLabel;
    edt743000: TEdit;
    edt709000: TEdit;
    Label27: TLabel;
    cb731000: TComboBox;
    cb730000: TComboBox;
    ch730000: TCheckBox;
    ch709000: TCheckBox;
    Label28: TLabel;
    cb750000: TComboBox;
    ch750000: TCheckBox;
    procedure Button1Click(Sender: TObject);
    procedure cb006000Change(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure gravarArquivo;
    procedure ordenar;
    procedure lerStatus;
    procedure adm;
    procedure cnf;
    procedure getDadosFormulario;
    procedure lerResp;
    procedure Imprimir;
    procedure cb732000Change(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure TimerGerarADMTimer(Sender: TObject);
    procedure mmStatusChange(Sender: TObject);
    procedure TimerLerRespTimer(Sender: TObject);
    procedure mmRespChange(Sender: TObject);
    procedure btCnfClick(Sender: TObject);
    procedure btnCncClick(Sender: TObject);
    procedure Button8Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPrincipal: TfrmPrincipal;
  //variáveis Req\intpos.001
  cod000x000  : String;  // Comando Onbrigatório
  cod001x000  : LongInt = -999;    //Identificação M n..10
  cod002x000  : String;     //Documento fiscal O n..12
  cod003x000  : Integer = -999;    //Valor total M n..12
  cod004x000  : Integer = -999;    //Moeda M n1
  cod006x000  : String;       //Entidade Cliente O a1
  cod007x000  : String;     //Identificador Cliente O  n..16
  cod009x000  : String;  // Status M para Resp a3
  cod010x000  : String;  //Rede Adquirente O a..8
  cod012x000  : String; //NSU M a..40
  cod013x000  : String;  //Código de autorização C8 a..6
  cod018x000  : Integer = -999;    //Qtde. parcelas C2 n1
  cod022x000  : String;    //Data no comprovante M n8
  cod023x000  : String;    //Hora no comprovante M n6
  cod024x000  : String;    //Data pré-datado C3 n8
  cod027x000  : String; //Código de controle M a..30
  cod028x000  : Integer; //Tamanho via única M n3
  cod029x000  : Integer = -999; //Código de controle M a..30
  cod702x000  : Integer = -999;    //Índice do Estabelecimento O n..2
  cod706x000  : Integer = -999;    //Capacidades da Automação M n..3
  cod707x000  : Integer = 0;
  cod708x000  : Integer = 0;
  cod709x000  : Integer = 0;
  cod716x000  : String; //Empresa da Automação M a..40
  cod717x000  : String;    //Data/hora fiscal O n12
  cod722x000  : String;     //Dados adicionais #1 O a..128
  cod723x000  : String;     //Dados adicionais #2 a..128
  cod724x000  : String;     //Dados adicionais #3 a..128
  cod725x000  : String;     //Dados adicionais #4 a..128
  cod726x000  : String[2];     //Idioma do Cliente O a2
  cod727x000  : String; //Taxa de serviço O n12
  cod728x000  : String;     //Taxa de embarque O n..12
  cod730x000  : Integer = -999;    //Operação O n..2
  cod731x000  : Integer = -999;    //Tipo de cartão O n..2
  cod732x000  : Integer = -999;    //Tipo de financiamento O n..2
  cod733x000  : Integer = -999;    //Versão da interface M n..3
  cod735x000  : String;     //Nome da Automação M a..40
  cod736x000  : String;     //Versão da Automação M a..20
  cod738x000  : String;     //Registro de Certificação M a..20
  cod739x000  : Integer = -999;    //Índice da Rede Adquirente O n3
  cod743x000  : Integer = 0;
  cod749x000  : Integer = -999;    //Forma de pagamento n..2
  cod750x000  : Integer = -999;    //Forma de idenficacao do portador da carteira digital n..3
  cod999x999  : Integer = -999;    //Registro finalizador  n1 = 0


  segundos : Integer;
  inteiro : Integer = 0;
  arquivoReqCriado : boolean = false;



  implementation

{$R *.dfm}

  procedure TfrmPrincipal.btnCncClick(Sender: TObject);
  var
    meuArquivo: TextFile;
    nomeArquivo: string;
    filetxt : TextFile;
    linha, diretorio: string;
    MainHandle: THandle;
    qtdarquivos : integer;
    Reco : TSearchRec;
    Procura, Contador:integer;
  begin
     cod000x000 := 'CNC';
     mmReqIntPos001.Clear;
     mmResp.Clear;
     mmStatus.Clear;
     diretorio := 'C:\PAYGO\Resp\';
     nomearquivo:= 'intpos.sts';
     arquivoReqCriado:= false;
     //AC apaga conteúdo da pasta Resp
     try
       DeleteFile('C:\PAYGO\Resp\intpos.001');
       DeleteFile('C:\PAYGO\Resp\intpos.sts');
     except

     end;

     inteiro := 0;
     while inteiro < 14 do
     begin
       inteiro := inteiro +1;
       lbStatus.Caption := inteiro.ToString;
       Application.ProcessMessages;
       sleep(250);
       if arquivoReqCriado = false then adm;
       sleep(250);

       try
         Contador:=0;
         procura:= FindFirst(Diretorio+ 'intPos.*', faAnyFile, Reco);
         Contador:=0;
         while procura = 0 do
         begin
           inc(Contador);
           procura:= FindNext(Reco)
         end;
         FindClose(Reco);
         //showmessage('contador arquivos: ' + contador.ToString);
       except

       end;

        //se houver arquivos na pasta realiza..
        if contador > 0 then
        begin
         lerStatus;
         //showmessage('lido');
         break;
         //inteiro := inteiro + 8; showmessage('Inteiro é 8 pq o arquivo já foi lido : ' + inteiro.ToString);
        end;
      end;

  end;


  procedure TfrmPrincipal.btCnfClick(Sender: TObject);
  begin
     cnf;

  end;

  procedure TfrmPrincipal.cnf;
  begin
    //limpar pasta de resposta
    lerStatus;
    try
      deletefile('C:\PAYGO\RESP\intpos.001');
    except
      lbStatus.Caption := 'não deu para apagar arq de resp';
    end;

    mmReqIntPos001.Clear;
    mmReqIntPos001.Lines.Add('000-000 = CNF');
    mmReqIntPos001.Lines.Add('001-000 = ' + cod001x000.ToString);
    if cod002x000 <> '' then    mmReqIntPos001.Lines.Add('002-000 = ' + cod002x000);
    mmReqIntPos001.Lines.Add('010-000 =' + cod010x000);

    mmReqIntPos001.Lines.Add('027-000 =' + cod027x000);
    mmReqIntPos001.Lines.Add('733-000 = ' + cod733x000.ToString);
    mmReqIntPos001.Lines.Add('735-000 = ' + cod735x000);
    mmReqIntPos001.Lines.Add('736-000 = ' + cod736x000);
    mmReqIntPos001.Lines.Add('738-000 = ' + cod738x000);
    mmReqIntPos001.Lines.Add('999-999 = 0');
  end;


procedure TfrmPrincipal.Button1Click(Sender: TObject);
  var
    meuArquivo: TextFile;
    nomeArquivo: string;
    filetxt : TextFile;
    linha, diretorio: string;
    MainHandle: THandle;
    qtdarquivos : integer;
    Rec : TSearchRec;
    Procura, Contador:integer;
  begin

    if cbOperacao.ItemIndex = 1 then
    cod000x000 := 'CRT'
    else  cod000x000 := 'ADM' ;
    //showmessage(cod000x000);

    mmReqIntPos001.Clear;
    mmResp.Clear;
    mmStatus.Clear;
    diretorio := 'C:\PAYGO\Resp\';
    nomearquivo:= 'intpos.sts';

    arquivoReqCriado:= false;
    //AC apaga conteúdo da pasta Resp
    try
      DeleteFile('C:\PAYGO\Resp\intpos.001');
      DeleteFile('C:\PAYGO\Resp\intpos.sts');
    except

    end;

    inteiro := 0;
    while inteiro < 14 do
    begin
      inteiro := inteiro +1;
      lbStatus.Caption := inteiro.ToString;
      Application.ProcessMessages;
      sleep(250);
      if arquivoReqCriado = false then adm;
      sleep(250);

      try
        Contador:=0;
        procura:= FindFirst(Diretorio+ 'intPos.*', faAnyFile, Rec);
        Contador:=0;
        while procura = 0 do
        begin
          inc(Contador);
          procura:= FindNext(Rec)
        end;
        FindClose(Rec);
        //showmessage('contador arquivos: ' + contador.ToString);
      except

      end;

      //se houver arquivos na pasta realiza..
      if contador > 0 then
      begin
        lerStatus;
        //showmessage('lido');
        break;
        //inteiro := inteiro + 8; showmessage('Inteiro é 8 pq o arquivo já foi lido : ' + inteiro.ToString);
      end;
      //lbStatus.Caption := inteiro.ToString;
      //TimerGerarADM.Enabled := true;

    end;

    lbStatus.Caption := 'Não encotnrou arquivo';
    //TimerGerarADM.Enabled := false;
    //segundos := 0;
  end;

  //


  procedure TfrmPrincipal.Button3Click(Sender: TObject);
  var filetxt : TextFile;
  linha, buffer: string;
  begin
      mmResp.Clear;
      lerResp;
  end;

  procedure TfrmPrincipal.lerResp;
  var
      F             : TStreamWriter;
      filetxt       : TextFile;
      linha, buffer : string;
      textoPrint    : String;
  begin

    textoPrint := '';
    AssignFile(filetxt,'C:\PAYGO\Resp\intpos.001');

    Reset(filetxt);
    //linha := '213';
    while not Eof(filetxt) do
    begin
      Readln(filetxt, linha);
      if linha <> '' then
        mmResp.Lines.Add(linha);

      if Pos('002-000',linha) > 0  then
      begin
        cod002x000 := Copy(linha, 10);
      end;

      if Pos('009-000',linha) > 0  then
      begin
        cod009x000 := Copy(linha, 10);

        if StrToInt(cod009x000) <> 0 then
        showmessage('Campo 009-000 = ' + cod009x000 + ' AC Acusa erro ');
        //cod027x000 := Copy(linha, 10);
      end;

      if Pos('012-000',linha) > 0  then
      begin
        cod012x000 := Copy(linha, 10);
      end;

      if Pos('729-000',linha) > 0  then
      begin
         //AC apaga conteudo da pasta resp/
         //AC Gera arquivo Req NCN
         //AC Aguarda arquivo Resp intpos.sts
         //Acusa Erro
         //Aponta para nova operação
         //Showmessage('Desenvolver');
      end;

      if Pos('029-',linha) > 0  then
       textoPrint := textoPrint + StringReplace(Copy(linha, 10) + #13 + #10, '"', '', [rfReplaceAll]) ;

       if Pos('028-000',linha) > 0  then
       cod028x000 := StrToInt(Copy(linha, 10));

      if Pos('027-000',linha) > 0  then
        cod027x000 := Copy(linha, 10);
        //showmessage('heyilioyurhey');

      if Pos('010-000',linha) > 0  then
         cod010x000 := Copy(linha, 10);

      if Pos('733-000',linha) > 0  then
        cod733x000 := StrToInt(Copy(linha, 10));


      // 009-000 igual a zero  e 729-00 está presente? Ou 729-00 está ausente e a transação gerou comprovantes? Então Houve sucesso, CNF e NCN devem ser habilitados
    end;
    CloseFile(filetxt);
    if cod028x000 > 0 then
    begin


     //impressão
     F := TStreamWriter.Create('\\localhost\MP-4200 TH', False, TEncoding.Default);
     try
      textoPrint := textoPrint + #10 +#13 + #27 + #109 + #1;  // 119 para corte total
      F.Write(textoPrint);
     finally
      F.Free;
     end;
     //fim da impressão

      //showmessage('cod028x000 é ' + cod028x000.ToString);
     sleep(3000);
     cod000x000 := 'CNF';
     mmReqIntPos001.Clear;
     cnf;
     gravarArquivo;
    end;

  end;

  procedure TfrmPrincipal.Button4Click(Sender: TObject);
  begin
    lerStatus;
  end;

  procedure TfrmPrincipal.Imprimir;
  begin
    //mmResp.
  end;

  procedure TfrmPrincipal.lerStatus;
  var filetxt : TextFile;
    linha, diretorio ,nomearquivo: string;
    MainHandle: THandle;
    qtdarquivos : integer;
    Rec : TSearchRec;
    Procura, Contador:integer;
  begin

    diretorio := 'C:\PAYGO\Resp\';
    nomearquivo:= 'intpos.sts';
    mmStatus.Lines.Clear;

    //conta arquivos na pasta
    try
      Contador:=0;
      procura:= FindFirst(Diretorio+ 'intPos.*', faAnyFile, Rec);
      Contador:=0;
      while procura = 0 do
      begin
        inc(Contador);
        procura:= FindNext(Rec)
      end;
      FindClose(Rec);
      //showmessage('contador arquivos: ' + contador.ToString);
    except

    end;

    //se houver arquivos na pasta realiza..
    if contador > 0 then
    begin

      //lendo arquivo de status e jogando no memo
      try
        AssignFile(filetxt, diretorio +nomearquivo);

        Reset(filetxt);

        while not Eof(filetxt) do
        begin
          Readln(filetxt, linha);
          if linha <> '' then
            mmStatus.Lines.Add(linha);
      end;
      except
          //inteiro := inteiro -4;
      end;

      // Esvaziar o cache (flush) imediatamente antes de fechar o arquivo;
      try
        MainHandle := OpenProcess(PROCESS_ALL_ACCESS, false, GetCurrentProcessID);
        SetProcessWorkingSetSize(MainHandle, $FFFFFFFF, $FFFFFFFF);
        CloseHandle(MainHandle);
      except showmessage('ss');
      end;

      try
        Application.ProcessMessages;
        CloseFile(filetxt);
        DeleteFile(diretorio+nomearquivo);
      except
        //inteiro := inteiro -4;  showmessage('sem exclusão');
      end;

      //inteiro := inteiro + 8; showmessage('Inteiro é 8 pq o arquivo já foi lido : ' + inteiro.ToString);
    end;
    //segundos := 999;

  end;


  procedure TfrmPrincipal.mmRespChange(Sender: TObject);
  begin
   TimerLerResp.Enabled := false;
  end;

  procedure TfrmPrincipal.mmStatusChange(Sender: TObject);
  begin
    //showmessage(mmResp.Lines.Count.ToString);
    if mmResp.Lines.Count = 0 then
    begin
       try
         //lerResp;
         TimerLerResp.Enabled := true;
       except
         //lbStatus.Caption := 'Arquivo de Resp não encontrado';
       end;

    end;

  end;

  procedure TfrmPrincipal.Button5Click(Sender: TObject);
  begin

    timer1.Enabled := true;

    try
      DeleteFile('C:\PAYGO\Resp\intpos.sts');
    except
      lbStatus.Caption := 'Não há arquivos para deleção.' ;
    end;

     mmReqIntPos001.Lines.Clear;
     mmReqIntPos001.Lines.Add('000-000 = ATV');     mmReqIntPos001.Lines.Add('001-000 = ' + edt001000.text);     mmReqIntPos001.Lines.Add('733-000 = ' + edt733000.text);     mmReqIntPos001.Lines.Add('738-000 = ' + edt738000.text);     mmReqIntPos001.Lines.Add('999-999 = 0');     gravarArquivo;     lbStatus.caption := 'Aguarde retorno...' ;

     lerStatus;
  end;

  procedure TfrmPrincipal.Button8Click(Sender: TObject);
  begin
    mmReqIntPos001.Lines.Add('000-000 = ' +	cod000x000);
    mmReqIntPos001.Lines.Add('001-000 = ' +	cod001x000.ToString);
    mmReqIntPos001.Lines.Add('006-000 = ' +	cod006x000);
    mmReqIntPos001.Lines.Add('706-000 = ' +	cod706x000.ToString);
    mmReqIntPos001.Lines.Add('733-000 = ' +	cod733x000.ToString);
    mmReqIntPos001.Lines.Add('735-000 = ' +	cod735x000);
    mmReqIntPos001.Lines.Add('736-000 = ' +	cod736x000);
    mmReqIntPos001.Lines.Add('738-000 = ' +	cod738x000);
    mmReqIntPos001.Lines.Add('999-000 = 0' );
  end;

  procedure TfrmPrincipal.getDadosFormulario;
  begin

    //limpar variaveis
    cod707x000 := 0;
    cod708x000 := 0;
    cod708x000 := 0;

      if cod000x000 <> 'CNC' then
      cod000x000  := cod000x000;  // Comando Onbrigatório

    try
      edt001000.Text := (StrToInt(edt001000.Text)+ 1).ToString;
      cod001x000   := StrToInt(edt001000.Text);    //Identificação M n..10
      //cod002x000  := edt;    //Documento fiscal O n..12

      cod003x000  := StrToInt(edt003000.Text) + StrToInt(edt708000.Text) - StrToInt(edt709000.Text) - StrToInt(edt743000.Text) ;    //Valor total M n..12

      cod004x000  := StrToInt(cb004000.Text[1]);    //Moeda M n1
      cod006x000  := cb006000.Text[1];       //Entidade Cliente O a1
      //cod007x000  := edt007000.Text;    //Identificador Cliente O  n..16
      cod010x000  := edt010000.Text;  //Rede Adquirente O a..8
      //cod012x000  := String[40]; //NSU O a..40
      //cod013x000  := edt;  //Código de autorização C8 a..6
      cod018x000  := StrToInt(edt018000.Text);    //Qtde. parcelas C2 n1
      cod022x000  := FormatDateTime('ddmmyyyy', Now);  //Data no comprovante M n8
      cod023x000  := StringReplace(FormatDateTime('hh:nn:ss', Now), ':', '', [rfReplaceAll]);   //Hora no comprovante M n6
      if edt024000.DateTime > now then cod024x000 := FormatDateTime('ddmmyyyy', edt024000.DateTime)
      else     cod024x000 := FormatDateTime('ddmmyyyy', Now);
      //scod024x000  := edt024000.DateTime;  showmessage(DateToStr(scod024x000));  //Data pré-datado C3 n8
      //cod027x000  := String[30]; //Código de controle O a..30
      cod702x000  := StrToInt(edt702000.Text);    //Índice do Estabelecimento O n..2
      cod706x000  := StrToInt(edt706000.Text);   //Capacidades da Automação M n..3
      cod707x000  := StrToInt(edt003000.Text);    //Valor original O n12
      cod708x000  := StrToInt(edt708000.Text);    //Valor troco O n12
      cod709x000  := StrToInt(edt709000.Text);    //Valor desconto O n12
      cod716x000  := edt716000.Text; //Empresa da Automação M a..40
      cod717x000  := FormatDateTime('yymmddhhmmss', Now);    //Data/hora fiscal O n12
      {cod722x000  := String;//Dados adicionais #1 O a..128
      cod723x000  := String;//Dados adicionais #2 a..128
      cod724x000  := String;//Dados adicionais #3 a..128
      cod725x000  := String;//Dados adicionais #4 a..128 }
      cod726x000  := 'pt';//Idioma do Cliente O a2
      //cod727x000  := Integer;    //Taxa de serviço O n12
      //cod728x000  := Integer;    //Taxa de embarque O n..12
      cod730x000  := StrToInt(cb730000.Text[1] + cb730000.Text[2]);     //Operação O n..2
      cod731x000  := StrToInt(cb731000.Text[1]);   //Tipo de cartão O n..2
      cod732x000  := StrToInt(cb732000.Text[1]);    //Tipo de financiamento O n..2
      cod733x000  := 1;    //Versão da interface M n..3
      cod735x000  := 'CaixaPayGo'; //Nome da Automação M a..40
      cod736x000  := 'V1.00'; //Versão da Automação M a..20
      cod738x000  := 'G45J35G3JH45B435'; //Registro de Certificação M a..20
      //cod739x000  := Integer;    //Índice da Rede Adquirente O n3
      cod749x000  := StrToInt(cb749000.Text[1]);    //Forma de pagtamento  O n..2
      cod750x000  := StrToInt(cb750000.Text[1] + cb750000.Text[2] + cb750000.Text[3]);    //Forma de pagtamento  O n..2
      cod999x999  := 0;    //Registro finalizador  n1 = 0 O  }
    except
      showmessage('Valor incorreto! Confira os campos!');
      lbStatus.Caption := 'Valor incorreto! Confira os campos!' ;  //Atualizando Status
      abort
    end;
  end;
  //
  procedure TfrmPrincipal.adm;
  var
    meuArquivo: TextFile;
    nomeArquivo: string;
  begin

    //validarcampos;
    lbStatus.Caption := 'Gerando arquivo Req' ;  //Atualizando Status

    getDadosFormulario;


    If(Trim(cod000x000) <> '') Then 	mmReqIntPos001.Lines.Add('000-000 = ' +	cod000x000);
    If(cod001x000 <> -999 )     Then 	mmReqIntPos001.Lines.Add('001-000 = ' +	cod001x000.ToString);
    If(Trim(cod002x000) <> '') Then 	mmReqIntPos001.Lines.Add('002-000 = ' +	cod002x000);
    If(cod003x000  <> -999)    Then 	mmReqIntPos001.Lines.Add('003-000 = ' +	cod003x000.ToString);
    If(cod004x000  <> -999)    Then 	mmReqIntPos001.Lines.Add('004-000 = ' +	cod004x000.ToString);
    //If(Trim(cod005x000) <> '') Then 	mmReqIntPos001.Lines.Add('005-000 = ' +	cod005x000);

    if cb006000.Enabled = true then
    begin
      If(Trim(cod006x000) <> '') Then 	mmReqIntPos001.Lines.Add('006-000 = ' +	cod006x000);
      If(Trim(cod007x000) <> '') Then 	mmReqIntPos001.Lines.Add('007-000 = ' +	cod007x000) else mmReqIntPos001.Lines.Add('007-000 = ' +	edt007000Cpf.text) ;
    end;

    //If(Trim(cod008x000) <> '') Then 	mmReqIntPos001.Lines.Add('008-000 = ' +	cod008x000);
    //If(Trim(cod009x000) <> '') Then 	mmReqIntPos001.Lines.Add('009-000 = ' +	cod009x000);
    if edt010000.Enabled = true then
    If(Trim(cod010x000) <> '') Then 	mmReqIntPos001.Lines.Add('010-000 = ' +	cod010x000) else showmessage('erro cod 010-000');
    //If(Trim(cod011x000) <> '') Then 	mmReqIntPos001.Lines.Add('011-000 = ' +	cod011x000);
    If(Trim(cod012x000) <> '') Then 	mmReqIntPos001.Lines.Add('012-000 = ' +	cod012x000);
    If(Trim(cod013x000) <> '') Then 	mmReqIntPos001.Lines.Add('013-000 = ' +	cod013x000);
    {If(Trim(cod014x000) <> '') Then 	mmReqIntPos001.Lines.Add('014-000 = ' +	cod014x000);
    If(Trim(cod015x000) <> '') Then 	mmReqIntPos001.Lines.Add('015-000 = ' +	cod015x000);
    If(Trim(cod016x000) <> '') Then 	mmReqIntPos001.Lines.Add('016-000 = ' +	cod016x000);
    If(Trim(cod017x000) <> '') Then 	mmReqIntPos001.Lines.Add('017-000 = ' +	cod017x000);}

    {If(Trim(cod019x000) <> '') Then 	mmReqIntPos001.Lines.Add('019-000 = ' +	cod019x000);
    If(Trim(cod020x000) <> '') Then 	mmReqIntPos001.Lines.Add('020-000 = ' +	cod020x000);
    If(Trim(cod021x000) <> '') Then 	mmReqIntPos001.Lines.Add('021-000 = ' +	cod021x000); }
    If(Trim(cod022x000) <> '') Then 	mmReqIntPos001.Lines.Add('022-000 = ' +	cod022x000);
    If(Trim(cod023x000) <> '') Then 	mmReqIntPos001.Lines.Add('023-000 = ' +	cod023x000);

    if edt024000.Enabled = true then
      If(Trim(cod024x000) <> '') Then 	mmReqIntPos001.Lines.Add('024-000 = ' +	cod024x000);
    //If(Trim(cod025x000) <> '') Then 	mmReqIntPos001.Lines.Add('025-000 = ' +	cod025x000);
    //If(Trim(cod026x000) <> '') Then 	mmReqIntPos001.Lines.Add('026-000 = ' +	cod026x000); }
    //If(Trim(cod027x000) <> '') Then 	mmReqIntPos001.Lines.Add('027-000 = ' +	cod027x000);
    //If(Trim(cod028x000) <> '') Then 	mmReqIntPos001.Lines.Add('028-000 = ' +	cod028x000);
    If(cod029x000  <> -999)    Then 	mmReqIntPos001.Lines.Add('729-000 = ' +	cod029x000.ToString);
    //If(Trim(cod030x000) <> '') Then 	mmReqIntPos001.Lines.Add('030-000 = ' +	cod030x000);
    //If(Trim(cod040x000) <> '') Then 	mmReqIntPos001.Lines.Add('040-000 = ' +	cod040x000);
    //If(Trim(cod700x000) <> '') Then 	mmReqIntPos001.Lines.Add('700-000 = ' +	cod700x000);
    //If(Trim(cod701x000) <> '') Then 	mmReqIntPos001.Lines.Add('701-000 = ' +	cod701x000); }
    if edt702000.Enabled = true then
      If(cod702x000  <> -999)    Then 	mmReqIntPos001.Lines.Add('702-000 = ' +	cod702x000.ToString);
    {If(Trim(cod703x000) <> '') Then 	mmReqIntPos001.Lines.Add('703-000 = ' +	cod703x000);
    If(Trim(cod704x000) <> '') Then 	mmReqIntPos001.Lines.Add('704-000 = ' +	cod704x000);
    If(Trim(cod705x000) <> '') Then 	mmReqIntPos001.Lines.Add('705-000 = ' +	cod705x000); }
    If(cod706x000  <> -999)    Then 	mmReqIntPos001.Lines.Add('706-000 = ' +	cod706x000.ToString);
    If(cod707x000 <> 0) Then 	mmReqIntPos001.Lines.Add('707-000 = ' +	cod707x000.toString);
    if(cod708x000 <> 0) Then 	mmReqIntPos001.Lines.Add('708-000 = ' +	cod708x000.toString);
    if(cod709x000 <> 0) Then 	mmReqIntPos001.Lines.Add('709-000 = ' +	cod709x000.toString);
    {If(Trim(cod710x000) <> '') Then 	mmReqIntPos001.Lines.Add('710-000 = ' +	cod710x000);
    If(Trim(cod711x000) <> '') Then 	mmReqIntPos001.Lines.Add('711-000 = ' +	cod711x000);
    If(Trim(cod712x000) <> '') Then 	mmReqIntPos001.Lines.Add('712-000 = ' +	cod712x000);
    If(Trim(cod713x000) <> '') Then 	mmReqIntPos001.Lines.Add('713-000 = ' +	cod713x000);
    If(Trim(cod714x000) <> '') Then 	mmReqIntPos001.Lines.Add('714-000 = ' +	cod714x000);
    If(Trim(cod715x000) <> '') Then 	mmReqIntPos001.Lines.Add('715-000 = ' +	cod715x000);}
    If(Trim(cod716x000) <> '') Then 	mmReqIntPos001.Lines.Add('716-000 = ' +	cod716x000);
    If(Trim(cod717x000) <> '') Then 	mmReqIntPos001.Lines.Add('717-000 = ' +	cod717x000);
    {If(Trim(cod718x000) <> '') Then 	mmReqIntPos001.Lines.Add('718-000 = ' +	cod718x000);
    If(Trim(cod719x000) <> '') Then 	mmReqIntPos001.Lines.Add('719-000 = ' +	cod719x000);
    If(Trim(cod720x000) <> '') Then 	mmReqIntPos001.Lines.Add('720-000 = ' +	cod720x000);
    If(Trim(cod721x000) <> '') Then 	mmReqIntPos001.Lines.Add('721-000 = ' +	cod721x000);}
    If(Trim(cod722x000) <> '') Then 	mmReqIntPos001.Lines.Add('722-000 = ' +	cod722x000);
    If(Trim(cod723x000) <> '') Then 	mmReqIntPos001.Lines.Add('723-000 = ' +	cod723x000);
    If(Trim(cod724x000) <> '') Then 	mmReqIntPos001.Lines.Add('724-000 = ' +	cod724x000);
    If(Trim(cod725x000) <> '') Then 	mmReqIntPos001.Lines.Add('725-000 = ' +	cod725x000);
    If(Trim(cod726x000) <> '') Then 	mmReqIntPos001.Lines.Add('726-000 = ' +	cod726x000);
    If(Trim(cod727x000) <> '') Then 	mmReqIntPos001.Lines.Add('727-000 = ' +	cod727x000);
    If(Trim(cod728x000) <> '') Then 	mmReqIntPos001.Lines.Add('728-000 = ' +	cod728x000);

    if cb730000.Enabled = true then
    begin
      If(cod730x000  <> -999)    Then 	mmReqIntPos001.Lines.Add('730-000 = ' +	cod730x000.ToString);
    end;

    if cb731000.Enabled = true then
    begin
      If(cod731x000  <> -999)    Then 	mmReqIntPos001.Lines.Add('731-000 = ' +	cod731x000.ToString);
    end;

    if cb732000.Enabled = true then
    begin
      if (cod018x000 = 0) then cod018x000 := 1; // 0 não é permitido
      If(cod018x000  <> -999)    Then 	mmReqIntPos001.Lines.Add('018-000 = ' +	cod018x000.ToString);
      If(cod732x000  <> -999)    Then 	mmReqIntPos001.Lines.Add('732-000 = ' +	cod732x000.ToString);
    end;

    If(cod733x000  <> -999)    Then 	mmReqIntPos001.Lines.Add('733-000 = ' +	cod733x000.ToString);
    //If(Trim(cod734x000) <> '') Then 	mmReqIntPos001.Lines.Add('734-000 = ' +	cod734x000);
    If(Trim(cod735x000) <> '') Then 	mmReqIntPos001.Lines.Add('735-000 = ' +	cod735x000);
    If(Trim(cod736x000) <> '') Then 	mmReqIntPos001.Lines.Add('736-000 = ' +	cod736x000);
    //If(Trim(cod737x000) <> '') Then 	mmReqIntPos001.Lines.Add('737-000 = ' +	cod737x000);
    If(Trim(cod738x000) <> '') Then 	mmReqIntPos001.Lines.Add('738-000 = ' +	cod738x000);
    If(cod739x000  <> -999)    Then 	mmReqIntPos001.Lines.Add('739-000 = ' +	cod739x000.ToString);
    //If(Trim(cod740x000) <> '') Then 	mmReqIntPos001.Lines.Add('740-000 = ' +	cod740x000);
    If(cod749x000 <> -999) Then 	mmReqIntPos001.Lines.Add('749-000 = ' +	cod749x000.toString);

    if cb750000.Enabled = true then
    begin
       If(cod750x000 <> -999) Then 	mmReqIntPos001.Lines.Add('750-000 = ' +	cod750x000.toString);
       showmessage(cod750x000.toString);
    end;

    mmReqIntPos001.Lines.Add('999-999 = ' +	cod999x999.ToString);

    ordenar;
    //gerar arquivo
    gravarArquivo;
    lbStatus.Caption := 'Sucesso! Aguardando resposta!' ;  //Atualizando Status
    arquivoReqCriado:= true;
  end;

  procedure TfrmPrincipal.Button9Click(Sender: TObject);
  begin

     //cb006000.onchange(cb006000);
    if ch010000.checked = true then
      edt010000.Enabled := true else  edt010000.Enabled := false;

    if ch730000.checked = true then
      cb730000.Enabled := true else   cb730000.Enabled := false;

    if ch731000.checked = true then
      cb731000.Enabled := true else   cb731000.Enabled := false;

    if ch750000.checked = true then
      cb750000.Enabled := true else   cb750000.Enabled := false;

    if ch702000.checked = true then
      edt702000.Enabled := true else  edt702000.Enabled := false;

    if ch708000.checked = true then
      edt708000.Enabled := true else  edt708000.Enabled := false;

    if ch709000.checked = true then
      edt709000.Enabled := true else  edt709000.Enabled := false;

    if ch006000.checked = true then
    begin
      cb006000.Enabled := true;
      cb006000.itemIndex  := 0;
      edt007000Cpf.Enabled := true;
      edt007000Cnpj.Enabled := false;
    end
    else
    begin
      cb006000.Enabled := false;
      cb006000.itemIndex  := 1;
      edt007000Cnpj.Enabled := false;
      edt007000Cpf.Enabled := false;
    end;

    if ch732000.checked = true then
    begin
      cb732000.Enabled := true;
      edt018000.Enabled := true;
    end
    else
    begin
      cb732000.Enabled := false;
      edt018000.Enabled := false;
    end;
  end;

  procedure TfrmPrincipal.cb006000Change(Sender: TObject);
  begin
    if cb006000.itemIndex = 0 then  // itemIndex = CPF
    begin
      edt007000Cnpj.Enabled := false;
      edt007000Cpf.Enabled := true;
      cod007x000  := edt007000Cpf.Text;    //Identificador Cliente O  n..16
    end
    else
    begin
      edt007000Cpf.Enabled := false;
      edt007000Cnpj.Enabled := true;
      cod007x000  := edt007000Cnpj.Text;    //Identificador Cliente O  n..16
    end;
  end;

  procedure TfrmPrincipal.cb732000Change(Sender: TObject);
  begin
    if (cb732000.itemindex = 4)  or (cb732000.itemindex = 5) then  edt024000.enabled := true
    else       edt024000.enabled := false;

  end;

  procedure TfrmPrincipal.gravarArquivo;
  var  f: TextFile; { declarando a variável "arq" do tipo arquivo texto }
     i, n: integer;
     MainHandle: THandle;
  begin
    //mmReqIntPos001.Clear;
    lbStatus.Caption := 'Gravando arquivo!' ;  //Atualizando Status
    AssignFile(f,'C:\PAYGO\Req\intpos.tmp');
    Rewrite(f); //abre o arquivo para escrita

    for i := 0 to mmReqIntPos001.Lines.Count - 1 do
    begin
      if Pos('Nome', mmReqIntPos001.Lines[i]) > 0 then
        Writeln(f,'Testando');// escreve no arquivo e desce uma linha
        //Write(f,'Sei lá onde' + #13 ); //escreve no arquivo sem descer a linha
        Write(f, mmReqIntPos001.Lines[i] + #13#10);
    end;

  // Esvaziar o cache (flush) imediatamente antes de fechar o arquivo;
    try
      MainHandle := OpenProcess(PROCESS_ALL_ACCESS, false, GetCurrentProcessID);
      SetProcessWorkingSetSize(MainHandle, $FFFFFFFF, $FFFFFFFF);
      CloseHandle(MainHandle);
    except showmessage('ss');
    end;
    Application.ProcessMessages;

    Closefile(f); //fecha o handle de arquivo

    RenameFile ('C:\PAYGO\Req\intpos.tmp', 'C:\PAYGO\Req\intpos.001');
    lbStatus.Caption := 'Arquivo gravado!' ;  //Atualizando Status
  end;

  procedure TFrmPrincipal.ordenar;
  var
    StrList: TStringList;
    i: integer;
  begin
    StrList:= TStringList.Create;
    for i:=0 to mmReqIntPos001.Lines.Count-1 do
      StrList.Add(mmReqIntPos001.Lines.Strings[i]);
      StrList.Sort;
      mmReqIntPos001.Lines.Clear;
      for i:=0 to StrList.Count-1 do
        mmReqIntPos001.Lines.Add(StrList.Strings[i]);
  end;


  procedure TfrmPrincipal.Timer1Timer(Sender: TObject);
  begin
    try
      lerStatus;
    except
      lbStatus.caption := 'Serviço INATIVO';
      timer1.enabled := false;
      exit;
    end;
    lbStatus.caption := 'Serviço Ativo';
    timer1.enabled := false;
  end;

  procedure TfrmPrincipal.TimerGerarADMTimer(Sender: TObject);
  begin
    //adm;
    try
      lerstatus;
    except
      //Showmessage('Not Deu');
    Segundos := segundos +9;
    end;
    Segundos := segundos +1;
  end;

  procedure TfrmPrincipal.TimerLerRespTimer(Sender: TObject);
  begin
    try
      lerResp;
    except

    end;
  end;

end.
